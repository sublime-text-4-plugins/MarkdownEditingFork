# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
events : TYPE
    Description
logger : TYPE
    Description
package_name : TYPE
    Description
plugin_name : str
    Description
root_folder : TYPE
    Description
settings : TYPE
    Description
"""
import os

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir)))
    )
)

import sublime_plugin  # noqa

from python_utils import logging_system
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

queue = Queue()
events = Events()

package_name = os.path.basename(root_folder)
plugin_name = "MarkdownEditingFork"
logger: logging_system.Logger = logging_system.Logger(
    logger_name=plugin_name,
    use_file_handler=os.path.join(root_folder, "tmp", "logs"),
)
settings = settings_utils.SettingsManager(events=events, logger=logger)


def set_logging_level():
    """Summary
    """
    try:
        logger.set_logging_level(logging_level=settings.get("mde.logging_level", "ERROR"))
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded():
    """Summary
    """
    queue.debounce(
        settings.load, delay=100, key=f"{plugin_name}-debounce-settings-load"
    )
    queue.debounce(
        set_logging_level, delay=200, key=f"{plugin_name}-debounce-set-logging-level"
    )


@events.on("plugin_unloaded")
def on_plugin_unloaded():
    """Summary
    """
    settings.unobserve()
    queue.unload()
    events.destroy()


@events.on("settings_changed")
def on_settings_changed(settings_obj, **kwargs):
    """Summary

    Parameters
    ----------
    settings_obj : TYPE
        Description
    **kwargs
        Description
    """
    if settings_obj.has_changed("mde.logging_level"):
        queue.debounce(
            set_logging_level, delay=100, key=f"{plugin_name}-debounce-settings-changed"
        )


if __name__ == "__main__":
    pass
