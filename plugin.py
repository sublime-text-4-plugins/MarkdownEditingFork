# -*- coding: utf-8 -*-
"""Load and Unload all MarkdownEditing modules.

This module exports __all__ modules, which Sublime Text needs to know about.
"""
import os

from .plugins import events
from .plugins import logger
from .plugins import root_folder
from .plugins import settings
from python_utils.sublime_text_utils import settings as settings_utils

all_plugins = sorted(
    [
        entry.name.replace(".py", "")
        for entry in os.scandir(os.path.join(root_folder, "plugins"))
        if all(
            (
                entry.is_dir() or entry.name.endswith(".py"),
                not entry.name.endswith("~"),
                not entry.name.startswith("_"),
            )
        )
    ]
)

for plugin in all_plugins:
    module = f".plugins.{plugin}"

    try:
        exec(f"from {module} import *")
    except Exception as err:
        logger.error(err)


class MarkdownEditingForkProjectSettingsController(settings_utils.ProjectSettingsController):
    """Project settings controller.
    """
    @settings_utils.distinct_until_buffer_changed
    def on_post_save_async(self, view):
        """Called after a view has been saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._on_post_save_async(view, settings)


def plugin_loaded():
    """On plugin loaded callback."""
    events.broadcast("plugin_loaded")


def plugin_unloaded():
    """On plugin unloaded."""
    events.broadcast("plugin_unloaded")
